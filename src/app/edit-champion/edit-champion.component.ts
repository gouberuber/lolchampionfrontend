import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-edit-champion',
  templateUrl: './edit-champion.component.html',
  styleUrls: ['./edit-champion.component.css']
})
export class EditChampionComponent implements OnInit {

  id = this.actRoute.snapshot.params['id']
  championDetails: any = {};

  constructor(
    private restApi: RestApiService,
    private actRoute: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.restApi.getChampion(this.id).subscribe((data: any) => {
      this.championDetails = data
    })
  }

  updateChampion(){
    if(window.confirm("Are you sure you want to update this champion")){
      this.restApi.updateChampion(this.id, this.championDetails).subscribe((data:any) => {
        this.router.navigate(['/'])
      })
    }
  }
}
