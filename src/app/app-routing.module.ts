import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateChampionComponent } from './create-champion/create-champion.component';
import { EditChampionComponent } from './edit-champion/edit-champion.component';
import { ListChampionsComponent } from './list-champions/list-champions.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'champion-list' },
  {path: 'champion-create', component: CreateChampionComponent},
  {path: 'champion-list', component: ListChampionsComponent},
  {path: 'champion-edit/:id', component: EditChampionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
