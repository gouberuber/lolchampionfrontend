import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Lolchampion } from '../shared/lolchampion';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-list-champions',
  templateUrl: './list-champions.component.html',
  styleUrls: ['./list-champions.component.css']
})
export class ListChampionsComponent implements OnInit {

  Champions: Array<Lolchampion> = [];

  constructor(
    private restApi: RestApiService,
  ) { }

  ngOnInit(): void {
    this.loadChampions()
  }

  loadChampions(){
    this.restApi.getChampions().subscribe((data: any) => {
      this.Champions = data
      console.log(this.Champions)
    })
  }
  deleteChampion(id: Number){
    if(window.confirm("Are you sure you want to delete this champion?")){
      this.restApi.deleteChampion(id).subscribe((_:any) => {
        this.loadChampions()
      })
    }
  }

}
