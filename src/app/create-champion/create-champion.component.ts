import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-create-champion',
  templateUrl: './create-champion.component.html',
  styleUrls: ['./create-champion.component.css']
})
export class CreateChampionComponent implements OnInit {

  Champion:any = {}

  constructor(
    private restApi: RestApiService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  createChampion(): void{
    if(window.confirm("Are you sure you want to create this champion?")){
      this.restApi.createChampion(this.Champion).subscribe((data: any) =>{
        this.router.navigate(['/'])
      })
    }
  }
}
