export class Lolchampion {
    constructor(){
        this.id=0;
        this.difficulty="";
        this.name="";
        this.role="";
        this.imageUrl="";
    }
    id: Number;
    difficulty: string;
    name: string;
    role: string;
    imageUrl: string;
}
