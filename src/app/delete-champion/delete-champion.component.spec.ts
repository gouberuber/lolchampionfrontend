import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteChampionComponent } from './delete-champion.component';

describe('DeleteChampionComponent', () => {
  let component: DeleteChampionComponent;
  let fixture: ComponentFixture<DeleteChampionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteChampionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteChampionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
