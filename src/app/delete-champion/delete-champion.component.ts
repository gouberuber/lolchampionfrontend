import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-delete-champion',
  templateUrl: './delete-champion.component.html',
  styleUrls: ['./delete-champion.component.css']
})
export class DeleteChampionComponent implements OnInit {
  
  id = this.actRoute.snapshot.params['id']


  constructor(
    private restApi: RestApiService,
    private actRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.restApi.deleteChampion(this.id).subscribe((data: any) => {
      this.router.navigate(['/'])
    })
  }



}
